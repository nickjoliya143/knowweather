


struct Constants{
    
    static let RegisterSegue = "RagisterToClima"
    static let LoginSegue = "LoginToClima"
    static let AppName = "⚡️KnowWeather"
    
    struct  Certificate {
        static let emptyCertificate = "Please enter Certificate Name"
        static let emptyDescription = "Please Enter Description"
        static let certificateUpdated = "Certificate updated successfully!"
        static let certificateAdded = "Certificate added successfully!"
    }
}

