//
//  SideMenuMainVC.swift
//  LearnEnglish
//
//  Created by mac on 23/11/22.
//

import UIKit

class SideMenuMainVC: UIViewController {
    
    
    var arrTblList = ["Share","Rate us","Privacy policy","Need Help"]
    var arrImg = [ "Share" , "review", "privacy-policy","ask"]

    @IBOutlet weak var tblSideMenu: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblSideMenu.delegate = self
        tblSideMenu.dataSource = self
        
        
        
    }
    
    

}


extension SideMenuMainVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTblList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let  cell =  tblSideMenu.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell"  ) as! SideMenuTableViewCell
        
    
        cell.lblTitle.text = arrTblList[indexPath.row]
        cell.imgSidemenu.image = UIImage(named: arrImg[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // let vc = self.storyboard?.instantiateViewController(withIdentifier: "LessonVC") as! LessonVC
        if indexPath.row == 0{
            
        }else if indexPath.row == 1{
            
            let text = URL(string: "https://www.apple.com")!
                let textShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            
        }else if indexPath.row == 2{
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            vc.isfromTC  = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
           
            let urlWhats = "whatsapp://send?phone=919687919263"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
                if let whatsappURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL){
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(whatsappURL)
                        }
                    }
                    else {
                        print("Install Whatsapp")
                    }
                }
            }
            
        }
       // self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
}

